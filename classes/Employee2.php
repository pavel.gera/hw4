<?php

class Employee2 extends User4
{
    private $name;
    private $surname;
    private $birthday;
    private $salary;

    public function __construct($name, $surname, string $birthday, $salary)
    {
        parent::__construct($name, $surname, $birthday);
        $this->salary = $salary;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getSalary()
    {
        return $this->salary;
    }
}
