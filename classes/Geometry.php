<?php


class Geometry
{
    private static $pi = 3.14; // вынесем Пи в свойство
    public $radius;

    public function getRadius() {
        return $this->radius;
    }
    public static function getCircleSquare($radius)
    {
        return self::$pi * $radius * $radius;
    }

    public static function getCircleCicuit($radius)
    {
        return 2 * self::$pi * $radius;
    }
    public static function getVolume($radius) {

        return (3/4) * self::$pi * pow($radius, 3);
    }

}