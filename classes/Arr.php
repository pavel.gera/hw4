<?php

class Arr
{
    protected $nums = [];
    private $sumHelper;
    private $AvgHelper;


    public function __construct() {
        $this->sumHelper = new SumHelper();
        $this->AvgHelper = new AvgHelper();
    }

    public function Add($num) {
        $this->nums[] = $num;
    }
    public function getSum23()
    {
        $nums = $this->nums;
        return ($this->sumHelper->getSum2($nums) + $this->sumHelper->getSum3($nums));
    }
    public function getMeanSquare() {
        return $this->getMeanSquare();
    }
    public function getAvgMeanSum() {
        $nums= $this->nums;
        return $this->AvgHelper->getAvg($nums) + $this->AvgHelper->getMeanSquare($nums);
    }
}