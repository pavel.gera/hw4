<?php

class FiguresCollection implements Figure2
{
private $figures = [];
private $a;
private $b;

public function __construct ($a, $b) {
    $this->a = $a;
    $this->b = $b;
}

public function addFigure (Figure2 $figure) {
    $this->figures[] = $figure;
}
public function getSquare()
{
    return $this->a * $this->b;
    // TODO: Implement getSquare() method.
}
public function getPerimeter()
{
    return ($this->a + $this->b) * 2;
    // TODO: Implement getPerimeter() method.
}
public function getTotalSquare () {
    $sum = 0;
    foreach ($this->figures as $item) {
        $sum += $item->getSquare;
    }
    return $sum;
}
}