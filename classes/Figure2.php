<?php

interface Figure2
{
    public function getSquare();
    public function getPerimeter();
}