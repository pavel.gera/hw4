<?php
require_once 'classes/Helper.php';
class Country

{
    private $name;
    private $age;
    private $population;
    use Helper;

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }


}