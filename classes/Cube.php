<?php

class Cube implements Figure3d
{
    private $a;

    /**
     * @param mixed $a
     */
    public function setA($a): void
    {
        $this->a = $a;
    }

    /**
     * @param mixed $b
     */

    public function getSquare() {
return pow($this->a, 3);
}
    public function getSurfaceSquare()
    {
        // TODO: Implement getSurfaceSquare() method.
        return (pow($this->a,2))*6;
    }

}