<?php
require_once 'classes/Trait1.php';
require_once  'classes/Trait2.php';
require_once  'classes/Trait3.php';


class Test5
{
use Trait1, Trait2, Trait3;
public function getSum () {
    return $this->method1() + $this->method2() + $this->method3();
}
}