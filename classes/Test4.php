<?php

class Test4
{
    public $prop1 = 1;
    public $prop2 = 2;
    private $prop3 = 3;
    private $prop4 = 4;

    public function test() {
        var_dump(get_object_vars($this));
    }
}