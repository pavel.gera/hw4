<?php

abstract class Figure
{
    abstract public function getSquare();
    abstract public function getPerimeter();
    abstract public function getSquarePerimeterSum();
}