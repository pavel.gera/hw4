<?php

class Driver extends Employee
{
private $experience;
private $category;

public function setExperience($experience) {
    $this->experience = $experience;
}
public function getExperience() {
    return $this->experience;
}
public function setCategory($category) {
    $this->category = $category;
}
 /**
 * @return mixed
 */public function getCategory()
{
    return $this->category;
}
}