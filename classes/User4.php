<?php

class User4
{
    private $name;
    private $surname;
    private $birthday;
    private $age;


    public function getName() {
        return $this->name;
    }
    public function getSurname() {
        return $this->surname;
    }
    public function getBirthday() {
        return $this->birthday;
        }

    public function getAge() {
        return $this->age;
    }
    public function __construct ($name, $surname, string $birthday) {
        $this->name = $name;
        $this->surname = $surname;
        $this->birthday = $birthday;
        $this->age = $this->calculateAge($birthday);
    }

    private function calculateAge($birthday) {
        $date = date('Y-m-d', strtotime($this->birthday));
        $new = date('ymd') - $date;
        return substr($new, 0, -4);
    }
}