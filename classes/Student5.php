<?php

class Student5 extends User7
{
    private $scholarship; // стипендия

    public function getScholarship()
    {
        return $this->scholarship;
    }

    public function setScholarship($scholarship)
    {
        $this->scholarship = $scholarship;
    }

    public function increaseRevenue($value)
    {
        $this->scholarship = $this->scholarship + $value;
    }
}