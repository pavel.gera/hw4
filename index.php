<?php

//Ex.1 Реализуйте классы User, Employee.
require_once 'classes/User.php';
require_once 'classes/Employee.php';
require_once 'classes/User2.php';
$user = new Employee();
$user->setName('John');
echo $user->getName() . '<br>';

//Ex.2 Реализуйте класс Student, наследующий от класса User
require_once 'classes/Student.php';

//Ex.3 Сделайте класс Programmer, который будет наследовать от класса Employee. Пусть новый класс имеет свойство langs,
// в котором будет хранится массив языков, которыми владеет программист. Сделайте также геттер и сеттер для этого свойства.
require_once 'classes/Programmer.php';
$programmer = new Programmer();
$programmer->setName('Tom');
$programmer->setLangs('php');
echo $programmer->getName() . '<br>' . $programmer->getLangs() . '<br>';

//Ex.4 Сделайте класс Driver (водитель), который будет наследовать от класса Employee.
// Пусть новый класс добавляет следующие свойства: водительский стаж, категория вождения (A, B, C, D), а также геттеры и сеттеры к ним.
require_once 'classes/Driver.php';
$driver = new Driver();
$driver->setCategory('A');
echo $driver->getCategory() . '<br>';

//Ex.5 Модифицируйте код класса User так, чтобы в методе setName выполнялась проверка того, что длина имени более 3-х символов.

$user = new User2();
$user->setName('To');
echo $user->getName() . '<br>';

//Ex.5 Добавьте в класс Student метод setName, в котором будет выполняться проверка того, что длина имени более 3-х символов и менее 10 символов.
// Пусть этот метод использует метод setName своего родителя, чтобы выполнить часть проверки.
$student = new Student();
$student->setName('Tomtomtomtom');
echo $student->getName() . '<br>';

//Ex.6 Реализуйте такой же класс Student, наследующий от User3.
require_once 'classes/User3.php';
require_once 'classes/Student2.php';
$student2 = new Student2('Tom', 25, 5);
echo $student2->getAge() . '<br>';
echo $student2->getCourse() . '<br>';

//Ex.7 Сделайте класс User, в котором будут следующие свойства только для чтения: name и surname.
// Начальные значения этих свойств должны устанавливаться в конструкторе. Сделайте также геттеры этих свойств.
require_once 'classes/User4.php';

//Ex.8 Модифицируйте предыдущую задачу так, чтобы третьим параметром в конструктор передавалась дата рождения работника в формате год-месяц-день.
// Запишите ее в свойство birthday. Сделайте геттер для этого свойства.
$user4 = new User4('Bob', 'Bobb', '1990-01-01'); // *** проставить формат даты в $birthday
echo $user4->getBirthday() . '<br>';

//Ex.9 Модифицируйте предыдущую задачу так, чтобы был приватный метод calculateAge, который параметром будет принимать дату рождения, а возвращать возраст с учетом того, был ли уже день рождения в этом году, или нет.
echo $user4->getAge() . '<br>';

//Ex.10 Сделайте класс Employee, который будет наследовать от класса User4. Пусть новый класс имеет свойство salary, в котором будет хранится зарплата работника. Зарплата должна передаваться четвертым параметром в конструктор объекта.
// Сделайте также геттер для этого свойства.
require_once 'classes/Employee2.php';
$employee2 = new Employee2('Tom', 'Tomm', '1990-10-10', 5000);
echo $employee2->getSalary() . '<br>';

//Ex.11 Самостоятельно повторите описанные мною классы Arr и SumHelper.
require_once 'classes/Arr.php';
require_once 'classes/SumHelper.php';
/*
$arr = new Arr();
$arr->Add(1);
$arr->Add(2);
$arr->Add(3);
echo $arr->getSum23() . '<br>';

//Ex.12 Создайте класс AvgHelper с методом getAvg, который параметром будет принимать массив и возвращать среднее арифметическое этого массива (сумма элементов делить на количество).
require_once 'classes/AvgHelper.php';


// Ex.13 Добавьте в класс AvgHelper еще и метод getMeanSquare, который параметром будет принимать массив и возвращать среднее квадратичное этого массива (квадратный корень, извлеченный из суммы квадратов элементов, деленной на количество).
echo $arr->getMeanSquare() . '<br>'; // *** Доработать*** http://code.mu/ru/php/book/oop/using-objects-in-class/ ***
*/
//Ex.14 Cделайте класс Employee с публичными свойствами name (имя) и salary (зарплата).
//Сделайте класс Student с публичными свойствами name (имя) и scholarship (стипендия).
//Создайте по 3 объекта каждого класса и в произвольном порядке запишите их в массив $arr.
require_once 'classes/Employee3.php';
require_once 'classes/Student3.php';
$object1 = new Employee3('Tom', 1000);
$object2 = new Employee3('Bob', 2000);
$object3 = new Employee3('John', 3000);
$student1 = new Student3('Tom2', 1500);
$student2 = new Student3('Tom3', 2500);
$student3 = new Student3('Tom4', 3500);

//Ex.15 Переберите циклом массив $arr и выведите на экран столбец имен всех работников
$arr2 = [$object2, $object1, $object3, $student2, $student3, $student1];
foreach ($arr2 as $nameObject) {
    if ($nameObject instanceof Employee3) {
        echo $nameObject->name . '<br>';
    }
 }

//Ex.16 Аналогичным образом выведите на экран столбец имен всех студентов.
foreach ($arr2 as $nameStudent) {
    if ($nameStudent instanceof Student3) {
        echo $nameStudent->name . '<br>';
    }
}

//Ex.17 Переберите циклом массив $arr и с его помощью найдите сумму зарплат работников и сумму стипендий студентов. После цикла выведите эти два числа на экран.
$salarySum = 0;
$scholarshipSum = 0;
foreach ($arr2 as $salaryObject) {
    if ($salaryObject instanceof Employee3) {
        $salarySum += $salaryObject->salary;
    }
}
    foreach ($arr2 as $salaryobject) {
    if ($salaryObject instanceof Student3) {
    $scholarshipSum += $salaryObject->salary;
    }
    }


Echo 'Сумма зарплат работников: ' . $salarySum . '<br>';
Echo 'Сумма стипендий студентов: ' . $scholarshipSum . '<br>';

//Ex.18 Сделайте класс User с публичным свойствами name и surname.
// Сделайте класс Employee, который будет наследовать от класса User и добавлять свойство salary.
// Сделайте класс City с публичными свойствами name и population.
require_once 'classes/User5.php';
require_once 'classes/Employee4.php';
require_once 'classes/City.php';

//Ex.19 Создайте 3 объекта класса User, 3 объекта класса Employee, 3 объекта класса City, и в произвольном порядке запишите их в массив $arr.
$usernew = new User5('Alex', 'Smith');
$usernew2 = new User5('Tom', 'Jones');
$usernew3 = new User5('Bob', 'Taylor');
$employee = new Employee4('Alex2', 'Jones2', 1000);
$employee2 = new Employee4('Tom2', 'Smith2', 2000);
$employee3 = new Employee4('Alex2', 'Taylor2', 3000);
$city = new City('Kiev', 3000000);
$city2 = new City('Paris', 4000000);
$city3 = new City('London', 5000000);

//Ex.20 Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов, которые принадлежат классу User или потомку этого класса.
$arr = [$usernew2, $usernew, $usernew3, $city, $city3, $city2, $employee3, $employee2, $employee];
foreach ($arr as $name) {
    if ($name instanceof User5) {
        echo $name->name . '<br>';
    }
}

//Ex.21 Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов, которые не принадлежат классу User или потомку этого класса.
foreach ($arr as $name2) {
    if ($name2 instanceof City)
        echo $name2->name . '<br>';
}

//Ex.22 Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов, которые принадлежат именно классу User, то есть не классу City и не классу Employee.
foreach ($arr as $name) {
    if($name instanceof User5 && !$name instanceof Employee4 && !$name instanceof City)
        echo $name->name . '<br>';
}

//Ex.23 Скопируйте мой код классов Employee и Student и самостоятельно не подсматривая в мой код реализуйте такой же класс UsersCollection.
require_once 'classes/Employee5.php';
require_once 'classes/Student4.php';
require_once 'classes/UsersCollection.php';
/*
$usercollection = new UsersCollection();
$usercollection->add(new Employee5('Tom', 1000));
$usercollection->add(new Employee5('Bob', 2000));
$usercollection->add(new Student4('Alex', 500));
$usercollection->add(new Student4('Paul', 800));

echo $usercollection->getTotalSalary() . '<br>'; // ***Доработать, выводит 0***
echo $usercollection->getTotalScholarship() . '<br>'; // ***Доработать, выводит 0***
echo $usercollection->getTotalPayment() . '<br>'; */

//Ex.24 Сделайте класс Post (должность), в котором будут следующие свойства, доступные только для чтения: name и salary.
// Создайте несколько объектов класса Post: программист, менеджер водитель.
require_once 'classes/Post.php';
$programmer = new Post('programmer',  1000);
$manager = new Post('manager',  2000);
$driver = new Post('driver', 3000);

//Ex.25 Сделайте класс Employee, в котором будут следующие свойства: name и surname.
// Пусть начальные значения этих свойств будут передаваться параметром в конструктор.
// Сделайте геттеры и сеттеры для свойств name и surname.
require_once 'classes/Employee6.php';

//Ex. 26 Пусть теперь третьим параметром конструктора будет передаваться должность работника, представляющая собой объект класса Post.
// Укажите тип этого параметра в явном виде.
// Создайте объект класса Employee с должностью программист. При его создании используйте один из объектов класса Post, созданный нами ранее.
// Выведите на экран имя, фамилию, должность и зарплату созданного работника.
$newEmployee = new Employee6('Tom', 'Jones', $programmer);
echo 'Имя работника: ' .  $newEmployee->getName() . '<br>';
echo 'Фамилия работника: ' . $newEmployee->getSurname() . '<br>';
echo 'Зарплата работника: ' . $newEmployee->getPost()->getSalary() . '<br>';

//Ex. 27 Реализуйте в классе Employee метод changePost, который будет изменять должность работника на другую.
// Метод должен принимать параметром объект класса Post. Укажите в методе тип принимаемого параметра в явном виде.
$newEmployee->changePost($manager);
echo 'Должность работника: ' . $newEmployee->getPost()->getName() . '<br>';

//Ex.28 Переделайте методы класса ArraySumHelper на статические.
require_once 'classes/ArraySumHelper.php';

//Ex.29 Пусть дан массив с числами. Найдите с помощью класса ArraySumHelper сумму квадратов элементов этого массива.
$arr = [1, 2, 3, 4, 5];
echo ArraySumHelper::getSum2($arr) . '<br>';

//Ex.30-1 Сделайте класс Num, у которого будут два публичных статических свойства: num1 и num2.
// Запишите в первое свойство число 2, а во второе - число 3. Выведите сумму значений свойств на экран.
require_once 'classes/Num.php';
Num::$num1 = 2;
Num::$num2 = 3;
echo (Num::$num1 + Num::$num2) . '<br>';

//Ex.30-2 Сделайте класс Num, у которого будут два приватны статических свойства: num1 и num2. Пусть по умолчанию в свойстве num1 хранится число 2, а в свойстве num2 - число 3.
// Сделайте в классе Num метод getSum, который будет выводить на экран сумму значений свойств num1 и num2.
require_once 'classes/Num2.php';
echo Num2::getSum() . '<br>';

//Ex.30-3 Добавьте в наш класс Geometry метод, который будет находить объем шара по радиусу. С помощью этого метода выведите на экран объем шара с радиусом 10.
require_once 'classes/Geometry.php';
$radius = 10;
echo Geometry::getVolume($radius) . '<br>';

//Ex.31-1 Реализуйте такой же класс User, подсчитывающий количество своих объектов.
require_once 'classes/User6.php';
$user = new User6('Tom');
echo User6::getCount() . '<br>';

//Ex.32 Сделайте объект какого-нибудь класса. Примените к объекту функцию get_class и узнайте имя класса, которому принадлежит объект.
$object = new User();
echo 'Class ' . get_class($object) . '<br>';

//Ex.33-1 Сделайте два класса: Test1 и Test2. Пусть оба класса имеют свойство name. Создайте некоторое количество объектов этих классов и запишите в массив $arr в произвольном порядке. Переберите этот массив циклом и для каждого объекта выведите значение его свойства name и имя класса, которому принадлежит объект.
require_once 'classes/Test1.php';
require_once 'classes/Test2.php';

$newtest = new Test1();
$newtest2 = new Test1();
$newtest3 = new Test2();
$newtest4 = new Test2();

$arr = [];
$arr[] = $newtest;
$arr[] = $newtest4;
$arr[] = $newtest3;
$arr[] = $newtest2;

foreach ($arr as $name) {
echo get_class($name) ." ".  $name->name . '<br>';
}

//Ex.33-3 Сделайте класс Test с методами method1, method2 и method3. С помощью функции get_class_methods получите массив названий методов класса Test.
require_once 'classes/Test3.php';
$class_methods =  get_class_methods('Test3');
foreach ($class_methods as $name_of_class) {
    echo $name_of_class . '<br>';
}

//Ex.33 Создайте объект класса Test, запишите его в переменную $test. С помощью функции get_class_methods получите массив названий методов объекта.
// Переберите его циклом и в цикле вызовите каждый метод класса, используя объект $test.
// Переберите этот массив циклом и в этом цикле вызовите каждый метод объекта.
$test = new Test3();
$newtest = get_class_methods($test);
foreach ($newtest as $method) {
echo $method . '<br>';
}

//Ex.34Сделайте класс Test с публичными свойствами prop1 и prop2, а также с приватными свойствами prop3 и prop4.
// Создайте объект этого класса. С помощью функции get_object_vars получите массив свойств созданного объекта.
require_once 'classes/Test4.php';
$prop = new Test4();
var_dump(get_object_vars($prop)) . '<br>';
// $prop->test();

//Ex.33 Реализуйте такой же абстрактный класс User, а также классы Employee и Student, наследующие от него.
require_once 'classes/User7.php';
require_once 'classes/Employee7.php';
require_once 'classes/Student5.php';
$employee = new Employee7();
$employee->setName('Joshua');
echo '<br>' . $employee->getName() . '<br>';

//Ex.33-2 Добавьте в ваш класс User такой же абстрактный метод increaseRevenue. Напишите реализацию этого метода в классах Employee и Student.
$employee->increaseRevenue(200);
$employee->setSalary(1000);
echo $employee->getSalary() . '<br>';

//Сделайте аналогичный класс Rectangle (прямоугольник), у которого будет два приватных свойства: $a для ширины и $b для длины.
// Данный класс также должен наследовать от класса Figure и реализовывать его метод.
// Добавьте в класс Figure метод getSquarePerimeterSum, который будет находить сумму площади и периметра.
require_once 'classes/Figure.php';
require_once 'classes/Rectangle.php';

$figure= new Rectangle();
$figure->setA(10);
$figure->setB(20);
echo $figure->getPerimeter() . '<br>';
echo $figure->getSquare() . '<br>';
echo $figure->getSquarePerimeterSum() . '<br>';

//Ex.36 Сделайте класс Disk (круг), реализующий интерфейс Figure.
require_once 'classes/Figure2.php';
require_once 'classes/Disk.php';
$figure = new Disk();
$figure->setRadius(10);
echo 'Площадь круга ' .  $figure->getSquare() . '<br>';

//Ex.37-1 Реализуйте класс FiguresCollection.
require 'classes/FiguresCollection.php';
$figure = new FiguresCollection(10,20);
$figure2 = new FiguresCollection(15, 20);
$figure->addFigure($figure);
$figure->addFigure($figure2);
$sum = new FiguresCollection(5, 5);
echo $figure->getTotalSquare() . '<br>';

//Ex.38 Пусть у нас дан интерфейс iUser. Сделайте класс User, который будет реализовывать данный интерфейс.
require_once 'classes/iUser.php';
require_once 'classes/User8.php';
$user = new User8;
$user->setName('Johny');
echo $user->getName() . '<br>';
$user->setAge(25);
echo $user->getAge() . '<br>';

//Ex.39 Сделайте интерфейс iUser, который будет описывать юзера. Предполагается, что у юзера будет имя и возраст и эти поля будут передаваться параметрами конструктора.
// Пусть ваш интерфейс также задает то, что у юзера будут геттеры (но не сеттеры) для имени и возраста.
// Сделайте класс User, реализующий интерфейс iUser.
require_once 'classes/iUser2.php';
require_once 'classes/User9.php';
$user = new User9('Bob', 30);
echo $user->getName() . " " . $user->getAge() . '<br>';

//Ex. 41 Сделайте интерфейс Figure3d (трехмерная фигура), который будет иметь метод getVolume (получить объем) и метод getSurfaceSquare (получить площадь поверхности
//Сделайте класс Cube, который будет реализовывать интерфейс Figure3d.
//Создайте несколько объектов класса Quadrate, несколько объектов класса Rectangle и несколько объектов класса Cube. Запишите их в массив $arr в случайном порядке.
//Переберите циклом массив $arr и выведите на экран только площади объектов реализующих интерфейс iFigure.
require_once 'classes/Figure3d.php';
require_once 'classes/Cube.php';
$figure = new Cube();
$figure2 = new Cube();
$figure3 = new Rectangle();
$figure4 = new Rectangle();
$figure->setA(5);
$figure2->setA(10);
$figure3->setA(1); $figure3->setB(2);
$figure4->setA(5); $figure4->setB(10);

$arr = [];
$arr[] = $figure;
$arr[] = $figure3;
$arr[] = $figure2;
$arr[] = $figure4;

foreach ($arr as $item) {
    if ($item instanceof Figure) {
        echo 'Square = ' . $item->getSquare() . '<br>';
    }
}

//Ex.44 Сделайте класс Sphere, который будет реализовывать интерфейс iSphere.
require_once 'classes/iSphere.php';
require_once 'classes/Sphere.php';
$disk = new Sphere(10);
echo 'Disk square = ' .  $disk->getSquare() . '<br>';

// Ex.46 Реализуйте класс Country со свойствами name, age, population и геттерами для них.
// Пусть наш класс для сокращения своего кода использует уже созданный нами трейт Helper.
require_once 'classes/Country.php';
require_once 'classes/Helper.php';
$city = new Country();
$city->setName('Kyiv');
echo $city->getName() . '<br>';


//Ex. 46-2 Сделайте 3 трейта с названиями Trait1, Trait2 и Trait3. Пусть в первом трейте будет метод method1, возвращающий 1, во втором трейте - метод method2, возвращающий 2, а в третьем трейте - метод method3, возвращающий 3.
// Пусть все эти методы будут приватными.
require_once 'classes/Trait1.php';
require_once 'classes/Trait2.php';
require_once 'classes/Trait3.php';

//Ex. 46-3 Сделайте класс Test, использующий все три созданных нами трейта.
// Сделайте в этом классе публичный метод getSum, возвращающий сумму результатов методов подключенных трейтов.
require_once 'classes/Test5.php';
$method = new Test5;
echo $method->getSum() . '<br>';

